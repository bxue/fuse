#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Description: Run for fuse-2.x CI gating test
#   Author: Zorro Lang <zlang@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TEST_DIR=/mnt/fuse2-gating

status=1
trap "cleanup;" 0 1 2 3 9 15

# test exits here
cleanup()
{
	pkill fusetest
	umount $TEST_DIR
	pkill fusexmp_fh 2>/dev/null
        if [ $status -eq 0 ];then
                _report "CLEAN" PASS
        else
		_report "CLEAN" FAIL
        fi
	exit $status
}

if [ -z "$RECIPEID" -o -z "$TASKID" ]; then
	OUTPUTFILE=`pwd`/output.log
	echo "No RECIPEID or TASKID, maybe local manual test" | tee $OUTPUTFILE
fi

function echoo()
{
        echo $@ | tee -a $OUTPUTFILE
}

# Wrapper to log the output of the command
function xlog()
{
	echoo "$@"
	$@ 2>&1 | tee -a $OUTPUTFILE
	return $?
}

# Wrapper to report_result, clears $OUTPUTFILE
function _report()
{
	local what="$FSTYP:$1"
	local status="$2"
	local score="$3"
	test -z "$score" && score=0
	echoo "---------------------------------------------"
	echoo -e "|Report:\t" "$what\t" "$status\t" "$score|"
	echoo "---------------------------------------------"
	rhts-report-result "$what" "$status" "$OUTPUTFILE" "$score"
	if [ -f "$OUTPUTFILE" ];then
		rm -f $OUTPUTFILE
		touch $OUTPUTFILE
	fi
}

function prepare_test()
{
	yum --enablerepo=* -y --nogpgcheck --setopt=skip_if_unavailable=True install fuse fuse-libs fuse-devel \
		pkgconf-pkg-config gcc glibc-headers procps-ng
	if [ $? -ne 0 ];then
		_report prepare_test FAIL
		exit 1
	fi

	_report prepare_test PASS
}

function build_test()
{
	xlog gcc -Wall fusexmp_fh.c `pkg-config fuse --cflags --libs` -lulockmgr -o fusexmp_fh
	if [ $? -ne 0 ];then
		_report build_test:fusexmp_fh FAIL
		exit 1
	fi
	xlog gcc -Wall fusetest.c -o fusetest
	if [ $? -ne 0 ];then
		_report build_test:fusetest FAIL
		exit 1
	fi

	_report build_test PASS
}

function do_test()
{
	mkdir -p $TEST_DIR 2>/dev/null
	if [ ! -d $TEST_DIR ];then
		_report do_test:TEST_DIR_missing FAIL
		exit 1
	fi
	xlog ./fusexmp_fh $TEST_DIR
	if [ $? -ne 0 ] || (! findmnt $TEST_DIR >/dev/null 2>&1 );then
		_report do_test:mount FAIL
		exit 1
	fi

	xlog ./fusetest $TEST_DIR
	if [ $? -eq 0 ];then
		_report do_test:fusetest PASS
	else
		_report do_test:fusetest FAIL
		exit 1
	fi
}

prepare_test
build_test
do_test
status=0
exit 0
